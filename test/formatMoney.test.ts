import { formatMoney } from '../src/index'

describe('formatMoney', () => {
  it('can provide a swedish format', () => {
    const result = formatMoney(100000, 'SEK', 'sv')

    expect(result).toBe('100 000 kr')
  })

  it('can provide a english format', () => {
    const result = formatMoney(100000, 'SEK', 'en')

    expect(result).toBe('SEK 100,000')
  })
})
