# @capcito/ui-money

This package contains money or currency related logic, based on TypeScript for UI development purpose.

## Use this package
In your `package.json`, just add the following as a regular `dependency`.
Please note the version at the end, which is controlled by git tags.
```
"@capcito/ui-money": "https://bitbucket.org/capcito/capcito-ui-money.git#v1.0.0"
```

## Create new version
In order to create a new version, you basically just run two commands, inside the master branch. But before doing so, you need to take a few things into consideration.

### Creating & Submitting a git tag
```
git tag -a v1.0.0 -m "Release 1.0.0"
git push origin v1.0.0
```

#### Major version
Does the following changes mean any breaking changes? If so, please bump the major version number, for example 1.2.0 > 2.0.0

#### Minor version
Does the following changes mean you're bringing new functionality without changing or breaking current functionality? If so, please bump the minor version number, for example 1.0.0 > 1.1.0

#### Patch version
Does the following changes just applies minor fixes, for example bug fixes, documentation or such, please bump the patch version number, for example 1.0.0 > 1.0.
