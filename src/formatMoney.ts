import * as accounting from 'accounting'

function getLocalizedSymbol (lang: string, currency: string) {
  if (currency === 'SEK' && lang === 'sv') {
    return 'kr'
  } else if (currency === 'SEK' && lang === 'en') {
    return 'SEK'
  }

  throw new Error(
    `The currency [${currency}] & culture [${lang}] is currently not supported`
  )
}

function getLocalizedFormat (lang: string) {
  switch (lang) {
    case 'sv_SE':
    case 'sv':
      return '%v %s'
    case 'en_US':
    case 'en':
      return '%s %v'
  }

  throw new Error(`The culture [${lang}] is currently not supported`)
}

function getLocalizedDecimal (lang: string) {
  switch (lang) {
    case 'sv_SE':
    case 'sv':
      return ','
    case 'en_US':
    case 'en':
      return '.'
  }

  throw new Error(`The culture [${lang}] is currently not supported`)
}

function getLocalizedThousand (lang: string) {
  switch (lang) {
    case 'sv_SE':
    case 'sv':
      return ' '
    case 'en_US':
    case 'en':
      return ','
  }

  throw new Error(`The culture [${lang}] is currently not supported`)
}

function getLocalizedPrecision () {
  return 0
}

/**
 * Based on the given arguments it will return a eye-friendly representation of the amount.
 *
 * Example: formatMoney(100000, SEK, sv_SE) => 100 000 kr
 */
export default function (amount: number, currency: string, lang: string) {
  const options = {
    currency: {
      symbol: getLocalizedSymbol(lang, currency),
      format: getLocalizedFormat(lang),
      decimal: getLocalizedDecimal(lang),
      thousand: getLocalizedThousand(lang),
      precision: getLocalizedPrecision()
    },
    number: {
      decimal: getLocalizedDecimal(lang),
      thousand: getLocalizedThousand(lang),
      precision: getLocalizedPrecision()
    }
  }

  accounting.settings.currency = options.currency
  accounting.settings.number = options.number

  return accounting.formatMoney(amount)
}
